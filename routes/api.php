<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::group(['middleware' => ['cors']], function () {
  //Rutas a las que se permitirá acceso
  
//});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});
Route::post('register', 'App\Http\Controllers\Auth\RegisterController@register');

Route::post('logout', 'App\Http\Controllers\Auth\LoginController@logout');
Route::post('password/{id}', 'App\Http\Controllers\UserController@updatePass');
Route::get('statsCategory', 'App\Http\Controllers\CategoryController@statsCategory');
Route::get('excelArticle', 'App\Http\Controllers\ArticleController@excel');
Route::apiResource('cliente', 'App\Http\Controllers\ClienteController' );
Route::apiResource('proveedor', 'App\Http\Controllers\ProveedorController' );
Route::apiResource('creditos', 'App\Http\Controllers\CreditoController' );
Route::apiResource('factura', 'App\Http\Controllers\FacturaController' );
Route::get('/credito', 'App\Http\Controllers\FacturaController@creditoFactura');
Route::apiResource('categoria', 'App\Http\Controllers\CategoryController' );
Route::apiResource('articulo', 'App\Http\Controllers\ArticleController' );
Route::post('factura-dates', 'App\Http\Controllers\FacturaController@twoDates');
Route::apiResource('articulo_factura', 'App\Http\Controllers\ArticlesFacturaController' );
Route::apiResource('caja', 'App\Http\Controllers\CajaController' );
Route::post('caja/salida/{id}', 'App\Http\Controllers\CajaController@salida' );
Route::post('caja/cerrar/{id}', 'App\Http\Controllers\CajaController@cerrar' );
Route::post('caja/entrada/{id}', 'App\Http\Controllers\CajaController@entrada' );
Route::apiResource('user', 'App\Http\Controllers\UserController');
Route::post('login', 'App\Http\Controllers\Auth\LoginController@login');