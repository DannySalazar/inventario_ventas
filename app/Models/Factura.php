<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Factura
 * 
 * @property int $id
 * @property int $total
 * @property int|null $cliente_id
 * @property int $user_id
 * @property Carbon $fecha_venta
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Cliente|null $cliente
 * @property User $user
 * @property Collection|Article[] $articles
 *
 * @package App\Models
 */
class Factura extends Model
{
	protected $table = 'facturas';

	protected $casts = [
		'total' => 'int',
		'cliente_id' => 'int',
		'user_id' => 'int'
	];

	protected $dates = [
		'fecha_venta'
	];

	protected $fillable = [
		'total',
		'cliente_id',
		'user_id',
		'fecha_venta'
	];

	public function cliente()
	{
		return $this->belongsTo(Cliente::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'table_articles_factura', 'factura_id', 'articulo_id')
					->withPivot('id', 'cantidad')
					->withTimestamps();
	}
}
