<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Proveedore
 * 
 * @property int $id
 * @property string $nombre
 * @property string $cedula_nit
 * @property string $apellido
 * @property string $direccion
 * @property string $celular
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Proveedore extends Model
{
	protected $table = 'proveedores';

	protected $fillable = [
		'nombre',
		'cedula_nit',
		'apellido',
		'direccion',
		'celular'
	];
}
