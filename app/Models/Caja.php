<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Caja
 * 
 * @property int $id
 * @property int|null $salidas
 * @property int|null $entradas
 * @property int|null $fin
 * @property int $inicio
 * @property int|null $reportado
 * @property int $user_id
 * @property string $estado
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property User $user
 *
 * @package App\Models
 */
class Caja extends Model
{
	protected $table = 'caja';

	protected $casts = [
		'salidas' => 'int',
		'entradas' => 'int',
		'fin' => 'int',
		'inicio' => 'int',
		'reportado' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'salidas',
		'entradas',
		'fin',
		'inicio',
		'reportado',
		'user_id',
		'estado'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
