<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cliente
 * 
 * @property int $id
 * @property string $nombre
 * @property string $cedula
 * @property string $apellido
 * @property string $direccion
 * @property string $celular
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|Factura[] $facturas
 *
 * @package App\Models
 */
class Cliente extends Model
{
	protected $table = 'clientes';

	protected $fillable = [
		'nombre',
		'cedula',
		'apellido',
		'direccion',
		'celular'
	];

	public function facturas()
	{
		return $this->hasMany(Factura::class);
	}
}
