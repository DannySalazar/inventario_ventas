<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

use Illuminate\Support\Str;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $api_token
 * @property Collection|Factura[] $facturas
 * @package App\Models
 */
class User extends Model implements AuthenticatableContract
{
	use Authenticatable;
	protected $table = 'users';

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'remember_token',
		'api_token'
	];

	protected $fillable = [
		'name',
		'email',
		'email_verified_at',
		'password',
		'remember_token',
		'api_token'
	];

	public function generateToken()
    {
        $this->api_token = Str::random(60);
        $this->save();

        return $this->api_token;
	}
	public function facturas()
	{
		return $this->hasMany(Factura::class);
	}
}
