<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 * 
 * @property int $id
 * @property string $nombre
 * @property string $codigo
 * @property int $precio_compra
 * @property int $precio_venta
 * @property string $descripcion
 * @property int $cantidad
 * @property int $precio_promedio
 * @property int $veces_ingresado
 * @property int $acumulado
 * @property int $category_id
 * @property float $iva
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Category $category
 * @property Collection|Factura[] $facturas
 *
 * @package App\Models
 */
class Article extends Model
{
	protected $table = 'articles';

	protected $casts = [
		'precio_compra' => 'int',
		'precio_venta' => 'int',
		'cantidad' => 'int',
		'precio_promedio' => 'int',
		'veces_ingresado' => 'int',
		'acumulado' => 'int',
		'category_id' => 'int',
		'iva' => 'float'
	];

	protected $fillable = [
		'nombre',
		'codigo',
		'precio_compra',
		'precio_venta',
		'descripcion',
		'cantidad',
		'precio_promedio',
		'veces_ingresado',
		'acumulado',
		'category_id',
		'iva'
	];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function facturas()
	{
		return $this->belongsToMany(Factura::class, 'table_articles_factura', 'articulo_id')
					->withPivot('id')
					->withTimestamps();
	}
}
