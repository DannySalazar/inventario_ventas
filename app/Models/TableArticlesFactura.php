<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TableArticlesFactura
 * 
 * @property int $id
 * @property int $factura_id
 * @property int $articulo_id
 * @property int $cantidad
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Article $article
 * @property Factura $factura
 *
 * @package App\Models
 */
class TableArticlesFactura extends Model
{
	protected $table = 'table_articles_factura';

	protected $casts = [
		'factura_id' => 'int',
		'articulo_id' => 'int',
		'cantidad' => 'int'
	];

	protected $fillable = [
		'factura_id',
		'articulo_id',
		'cantidad'
	];

	public function article()
	{
		return $this->belongsTo(Article::class, 'articulo_id');
	}

	public function factura()
	{
		return $this->belongsTo(Factura::class);
	}
}
