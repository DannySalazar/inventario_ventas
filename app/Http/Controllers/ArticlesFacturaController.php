<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\TableArticlesFactura;
class ArticlesFacturaController extends Controller
{
    public function index(Request $request)
    {
        $articles = TableArticlesFactura::with('article','factura')->orderBy('id');

        

        $articles = $articles->get()->toArray();
        return response()->json([
            'status' => true,
            'message' => 'Articulos obtenidos exitosamente.',
            'data' => ['articles' => $articles]
        ]);


    }
}
