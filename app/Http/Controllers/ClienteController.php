<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clientes = Cliente::orderBy('apellido');

        if($request->search){
            $clientes->where('cedula', $request->search);
        }

        $clientes = $clientes->get()->toArray();
        return response()->json([
            'status' => true,
            'message' => 'Clientes obtenidos exitosamente.',
            'data' => ['clientes' => $clientes]
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $cliente = new Cliente;
        $cliente->nombre = $request->nombre;
        $cliente->cedula = $request->cedula;
        $cliente->apellido = $request->apellido;
        $cliente->direccion = $request->direccion;
        $cliente->celular = $request->celular;
        $cliente->save();

        return response()->json([
            'status' => true,
            'message' => 'Cliente creado exitosamente',
            'data' => ['cliente' => $cliente->toArray()]
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clientes = Cliente::where('id', $id)->get()->toArray();

        return response()->json([
            'status' => true,
            'message' => 'Cliente obtenidos exitosamente.',
            'data' => ['cliente' => $clientes]
        ]);
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        $cliente->nombre = $request->nombre;
        $cliente->cedula = $request->cedula;
        $cliente->apellido = $request->apellido;
        $cliente->direccion = $request->direccion;
        $cliente->celular = $request->celular;
        $cliente->save();

        return response()->json([
            'status' => true,
            'message' => 'Cliente actualizado exitosamente',
            'data' => ['cliente' => $cliente->toArray()]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cliente = Cliente::find($id);
            $cliente->delete();

            return response()->json([
                'status' => true,
                'message' => 'Cliente eliminado exitosamente'
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status' => false,
                'message' => 'El Cliente esta en uso, no es posible eliminar'
            ], 423);
        }
    }
}
