<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\Article;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ArticleExport; 

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $articles = Article::with('category')->orderBy('nombre');

        if ($request->search) {
            $articles->where('codigo', $request->search);
            $articles->orWhere('nombre','like','%' . $request->search. '%');
        }
        

        if($request->notificaciones)
        {
            $articles->where('cantidad','<=', $request->notificaciones);
        }
        
        

        $articles = $articles->get()->toArray();
        return response()->json([
            'status' => true,
            'message' => 'Articulos obtenidos exitosamente.',
            'data' => ['articles' => $articles]
        ]);


    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = new Article;
        $article->nombre = $request->nombre;
        $article->codigo = $request->codigo;
        $article->precio_compra = $request->precio_compra;
        $article->precio_venta = $request->precio_venta;
        $article->descripcion = $request->descripcion;
        $article->cantidad = $request->cantidad;
        $article->precio_promedio = $request->precio_compra;
        $article->veces_ingresado = 1;
        $article->category_id = $request->category_id;
        $article->acumulado = $request->precio_compra;
        $article->iva = $request->iva;
        $article->save();

        return response()->json([
            'status' => true,
            'message' => 'Articulo creado exitosamente',
            'data' => ['article' => $article->toArray()]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $articles = Article::with('category')->where('id', $id)->get()->toArray();

        return response()->json([
            'status' => true,
            'message' => 'Articulo obtenido exitosamente.',
            'data' => ['article' => $articles]
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $article->nombre = $request->nombre;
        $article->codigo = $request->codigo;
        $article->precio_compra = $request->precio_compra;
        $article->precio_venta = $request->precio_venta;
        $article->descripcion = $request->descripcion;
        $article->cantidad += $request->cantidad;
        $article->precio_promedio = ($article->acumulado + $request->precio_compra)/($article->veces_ingresado + 1) ;
        $article->veces_ingresado += 1;
        $article->acumulado += $request->precio_compra;
        $article->iva = $request->iva;
        $article->save();

        return response()->json([
            'status' => true,
            'message' => 'Articulo actualizado exitosamente',
            'data' => ['article' => $article->toArray()]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $article = Article::find($id);
            $article->delete();

            return response()->json([
                'status' => true,
                'message' => 'Articulo eliminado exitosamente'
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status' => false,
                'message' => 'El articulo esta en uso, no es posible eliminar'
            ], 423);
        }
    }

    public function excel()
    {
        return Excel::download(new ArticleExport, 'pocos-productos.xlsx');
    }
}
