<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\Factura;
use App\Models\Article;
use App\Models\Cliente;
use App\Models\Caja;
use App\Models\TableArticlesFactura;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FacturaController extends Controller
{
    public function index(Request $request)
    {
        
        $hoy = Carbon::now();
        

        $articles = Factura::with('cliente', 'articles', 'user')->orderBy('fecha_venta');

        if ($request->today) {
            $articles->whereDate('fecha_venta', Carbon::today());
        }
        if($request->week){
            Carbon::setWeekStartsAt(Carbon::SUNDAY);
            Carbon::setWeekEndsAt(Carbon::SATURDAY);
            $articles->whereBetween('fecha_venta', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()]);
        }
        if ($request->month) {
            $articles->whereMonth('fecha_venta', $hoy->month);
        }

        $articles = $articles->get()->toArray();
        return response()->json([
            'status' => true,
            'message' => 'Facturas obtenidas exitosamente.',
            'data' => ['articles' => $articles],
        ]);


    }

    public function store(Request $request)
    {
        $factura = new Factura;
        $factura->cliente_id = $request->cliente;
        $factura->total = $request->total;
        $factura->fecha_venta = $request->fecha_venta;
        $factura->user_id = $request->vendedor;
        $factura->credito = $request->credito;
        $factura->save();
        if($request->credito == 0)
        {
            self::addCaja($request->idCaja, $request->total);
        }
        self::guardarProductos($factura->id, $request->articulos);
        
    }

    public function addCaja($id, $total)
    {
        $caja = Caja::find($id);
        $caja->entradas += $total;
        $caja->save();
    }

    public function guardarProductos($id, $articulos){
        
        $estado = self::actualizarCantidades($articulos);
        if($estado)
        {
            for($i=0; $i<count($articulos); $i++)
            {
                $af = new TableArticlesFactura;
                $af->factura_id = $id;
                $af->articulo_id = $articulos[$i]['id'];
                $af->cantidad = $articulos[$i]['cantidad'];
                $af->save();
                
            }
        }
        else{
            return response()->json([
                'status' => true,
                'message' => 'No hay suficientes candidades de un producto',
            ], 403);
        }
        return response()->json([
            'status' => true,
            'message' => 'Factura guardada exitosamente.',
        ]);
        
    }

    public function actualizarCantidades($articulos)
    {
        for($i=0; $i<count($articulos); $i++)
        {
            $article = Article::find($articulos[$i]['id']);
            $article->cantidad -=  $articulos[$i]['cantidad'];
            $article->save();
            
        }
        return true;
    }
    
    public function show(int $id)
    {
        $articles = Factura::with('cliente', 'articles', 'user')->where('id', $id)->orderBy('fecha_venta');
        $articles = $articles->get()->toArray();
        return response()->json([
            'status' => true,
            'message' => 'Facturas obtenidas exitosamente.',
            'data' => ['articles' => $articles],
        ]);
    }

    public function twoDates(Request $request)
    {
        $t1 = strtotime($request->date1);
        $t2 = strtotime($request->date2);
        $articles = Factura::with('cliente', 'articles', 'user')
        ->whereBetween('fecha_venta',[Carbon::parse($request->date1), Carbon::parse($request->date2)] )
        ->get()->toArray();

        return response()->json([
            'status' => true,
            'message' => 'Facturas obtenidas exitosamente.',
            'data' => ['articles' => $articles],
        ]);
    }

    public function creditoFactura()
    {    
        $deuda = DB::table('facturas')
        ->select('facturas.cliente_id', DB::raw('sum(facturas.total) as deuda'))
        ->groupBy('facturas.cliente_id')
        ->where('facturas.credito',1);

        $totalPagos = DB::table('credito')
        ->select('credito.cliente_id', DB::raw('sum(credito.cuota) as pagos'))
        ->groupBy('credito.cliente_id');
        

        $credito = Cliente::select("clientes.id","clientes.nombre", "clientes.cedula", "clientes.apellido", "deuda.deuda", "totalPagos.pagos")
        ->leftJoinSub($deuda,"deuda", function($join){
            $join->on("clientes.id","=", "deuda.cliente_id");
        })
        ->leftJoinSub($totalPagos,"totalPagos", function($join){
            $join->on("clientes.id","=", "totalPagos.cliente_id");
        })->get()->toArray();
        

        

        return response()->json([
            'status' => true,
            'message' => 'sum deura',
            'data' => ['stats' => $credito]
        ]);

    }

    
}
