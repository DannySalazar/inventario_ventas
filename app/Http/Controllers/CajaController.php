<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\Caja;
class CajaController extends Controller
{
    public function index(Request $request)
    {
        $caja = Caja::with('user')->orderBy('created_at');

        if($request->usuario){
            $caja = $caja->where('user_id', $request->usuario)->where('estado','abierta')->first();
            return response()->json([
                'status' => true,
                'message' => 'Caja obtenidos exitosamente.',
                'data' => ['caja' => $caja]
            ]);
        }

        $caja = $caja->get()->toArray();

        return response()->json([
            'status' => true,
            'message' => 'Caja obtenidos exitosamente.',
            'data' => ['caja' => $caja]
        ]);


    }

    public function store(Request $request)
    {
        $caja = new Caja;
        $caja->inicio = $request->inicial;
        $caja->user_id = $request->user_id;
        $caja->estado = "abierta";
        $caja->salidas = 0;
        $caja->entradas = 0;
        $caja->fin = 0;
        $caja->reportado =0;
        $caja->save();

        return response()->json([
            'status' => true,
            'message' => 'Caja guardada exitosamente.',
            'data' => ['caja' => $caja]
        ]);

    }

    public function salida(Request $request, $id){
        $caja = Caja::find($id);
        $caja->salidas += $request->salida;
        $caja->save();
        return response()->json([
            'status' => true,
            'message' => 'Salida guardada exitosamente.',
            'data' => ['caja' => $caja]
        ]);
    }

    public function entrada(Request $request, $id){
        $caja = Caja::find($id);
        $caja->entradas += $request->entrada;
        $caja->save();
        return response()->json([
            'status' => true,
            'message' => 'Salida guardada exitosamente.',
            'data' => ['caja' => $caja]
        ]);
    }

    public function cerrar(Request $request, $id)
    {
        $caja = Caja::find($id);
        $caja->fin = ($caja->inicio + $caja->entradas - $caja->salidas);
        $caja->reportado = $request->reportado;
        $caja->estado = "cerrada";  
        $caja->save();

        return response()->json([
            'status' => true,
            'message' => 'Caja cerrada exitosamente.',
            'data' => ['caja' => $caja]
        ]);
    }
}
