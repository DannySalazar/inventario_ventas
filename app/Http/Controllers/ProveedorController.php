<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\Proveedore;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provedores = Proveedore::orderBy('apellido')->get()->toArray();

        return response()->json([
            'status' => true,
            'message' => 'Proveedores obtenidos exitosamente.',
            'data' => ['proveedores' => $provedores]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proveedor = new Proveedore;
        $proveedor->nombre = $request->nombre;
        $proveedor->cedula_nit = $request->cedula_nit;
        $proveedor->apellido = $request->apellido;
        $proveedor->direccion = $request->direccion;
        $proveedor->celular = $request->celular;
        $proveedor->save();

        return response()->json([
            'status' => true,
            'message' => 'Proveedor creado exitosamente',
            'data' => ['proveedor' => $proveedor->toArray()]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proveedores = Proveedore::orderBy('apellido')->where('id', $id)->get()->toArray();

        return response()->json([
            'status' => true,
            'message' => 'Proveedor obtenido exitosamente.',
            'data' => ['proveedor' => $proveedores]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proveedor = Proveedore::find($id);
        $proveedor->nombre = $request->nombre;
        $proveedor->cedula_nit = $request->cedula_nit;
        $proveedor->apellido = $request->apellido;
        $proveedor->direccion = $request->direccion;
        $proveedor->celular = $request->celular;
        $proveedor->save();

        return response()->json([
            'status' => true,
            'message' => 'Proveedor Actualizado exitosamente',
            'data' => ['proveedor' => $proveedor->toArray()]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $proveedor = Proveedore::find($id);
            $proveedor->delete();

            return response()->json([
                'status' => true,
                'message' => 'Proveedor eliminado exitosamente'
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status' => false,
                'message' => 'El Proveedor esta en uso, no es posible eliminar'
            ], 423);
        }
    }
}
