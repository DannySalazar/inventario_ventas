<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\Category;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('nombre')->get()->toArray();

        return response()->json([
            'status' => true,
            'message' => 'categorias obtenidas exitosamente.',
            'data' => ['categories' => $categories]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        $category->nombre = $request->nombre;
        $category->save();

        return response()->json([
            'status' => true,
            'message' => 'categoria creada exitosamente',
            'data' => ['category' => $category->toArray()]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $categories = Category::where('id', $id)->get()->toArray();

        return response()->json([
            'status' => true,
            'message' => 'categoria obtenida exitosamente.',
            'data' => ['category' => $categories]
        ]);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category =  Category::find($id);
        $category->nombre = $request->nombre;
        $category->save();

        return response()->json([
            'status' => true,
            'message' => 'categoria actualizada exitosamente',
            'data' => ['categoria' => $category->toArray()]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = Category::find($id);
            $category->delete();

            return response()->json([
                'status' => true,
                'message' => 'Categoria eliminada exitosamente'
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status' => false,
                'message' => 'La categoria esta en uso, no es posible eliminar'
            ], 423);
        }
    }

    public function statsCategory()
    {    
        $categories = DB::table('categories')
        ->select('categories.nombre', DB::raw('sum(table_articles_factura.cantidad) as vendidos'))
        ->join('articles', 'articles.category_id', 'categories.id')
        ->join('table_articles_factura', 'table_articles_factura.articulo_id', 'articles.id')
        ->groupBy('categories.nombre')
        ->get()->toArray();

        return response()->json([
            'status' => true,
            'message' => 'sum categoria',
            'data' => ['stats' => $categories]
        ]);

    }
}
