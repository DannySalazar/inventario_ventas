<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $user = User::select("*")->get()->toArray();
        return response()->json([
            'status' => true,
            'message' => 'Usuarios obtenidos exitosamente.',
            'data' => ['usuarios' => $user],
        ]);
    }

    public function show(int $id)
    {
        $user = User::where('id',$id)->get()->toArray();
        return response()->json([
            'status' => true,
            'message' => 'Usuarios obtenidos exitosamente.',
            'data' => ['usuarios' => $user],
        ]);
    }

    public function updatePass(Request $request, $id)
    {
        
        if($request->password == $request->confirm_password)
        {
            $user = User::find($id);
            $user->password = Hash::make($request->password);
            $user->save();

            return response()->json([
                'status' => true,
                'message' => 'Cambio de Clave exitosamente.',
                'data' => ['usuarios' => $user],
            ]);
        }
        else{
            return response()->json([
                'status' => false,
                'message' => 'Error de contraseña exitosamente.',   
            ], 403);
        }
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->save();
        return response()->json([
            'status' => true,
            'message' => 'Usuarios Editado exitosamente.',
            'data' => ['usuarios' => $user],
        ]);
    }

}
