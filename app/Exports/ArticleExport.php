<?php

namespace App\Exports;

use App\Models\Article;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ArticleExport implements FromCollection, WithHeadings
{

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Article::select("nombre","codigo","descripcion","cantidad")->where('cantidad','<=', 2)->get();
    }
    public function headings(): array
    {
        return [
            "Nombre del Producto",
            "Codigo",
            "Descripcion",
            "Cantidad"
        ];
    }
}
